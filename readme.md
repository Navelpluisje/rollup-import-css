# Just another CSS plugin for Rollup

Currently this plugin does nothing more then passing the content of the css file.
There will be a chance a postprocessor will be added. Bu right now it does wath I wanted it to do for a project of mine.

## Using the plugin

* Run `npm i -D @navelpluisje/rollup-plugin-css`.
* Import in your `rollup.config.js` file: `import css from '@navelpluisje/rollup-plugin-css'`
* Ad the css plugin to your plugins. There are no options yet
```javascript
  plugins: [
    css(),
  ],
```

## Using with typescript

Add a `*.d.ts` file to your project with at least this snippet:
```javascript
declare module "*.css" {
  const value: string;
  export default value;
}
```