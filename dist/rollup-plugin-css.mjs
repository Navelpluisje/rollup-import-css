import { createFilter } from 'rollup-pluginutils';

function string(options) {
  if ( options === void 0 ) options = {};

  if (!options.include) {
    options.include = '**/*.css';
  }

  var filter = createFilter(options.include, options.exclude);

  return {
    name: 'rollup-import-css',

    transform: function transform(code, id) {
      if (filter(id)) {
        var css = {
          code: ("export default " + (JSON.stringify(code)) + ";"),
          map: { mappings: '' }
        };

        return css;
      }
    }
  };
}

export default string;
