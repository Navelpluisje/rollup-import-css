import { createFilter } from 'rollup-pluginutils';

export default function string(options = {}) {
  if (!options.include) {
    options.include = '**/*.css'
  }

  const filter = createFilter(options.include, options.exclude);

  return {
    name: 'rollup-import-css',

    transform(code, id) {
      if (filter(id)) {
        const css = {
          code: `export default ${JSON.stringify(code)};`,
          map: { mappings: '' }
        };

        return css;
      }
    }
  };
}
